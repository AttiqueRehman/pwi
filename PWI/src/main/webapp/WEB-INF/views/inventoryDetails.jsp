<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
<style>
.blue-button {
	background: #25A6E1;
	filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#25A6E1',
		endColorstr='#188BC0', GradientType=0);
	padding: 3px 5px;
	color: #fff;
	font-family: 'Helvetica Neue', sans-serif;
	font-size: 12px;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 4px;
	border: 1px solid #1A87B9
}

table {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	width: 50%;
}

th {
	background: SteelBlue;
	color: white;
}

td, th {
	border: 1px solid gray;
	width: 25%;
	text-align: left;
	padding: 5px 10px;
}
</style>
</head>
<body>
	<form:form method="post" modelAttribute="inventory"
		action="/PWI/addInventory">
		<table>
			<tr>
				<th colspan="2">Add Inventory</th>
			</tr>
			<tr>
				<td><form:label path="warehouse">Warehouse:</form:label></td>
				<td><form:input path="warehouse" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<form:hidden path="id" />
				<td><form:label path="itemName">Item Name:</form:label></td>
				<td><form:input path="itemName" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="type">Product Type:</form:label></td>
				<td><select name="type" name="productList"
					id="productList">
						<option value=""></option>
						<c:forEach items="${productList}" var="option">
							<option value="${option}">
								<c:out value="${option.type}"></c:out>
							</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td><form:label path="size">Size:</form:label></td>
				<td><form:input path="size" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="inStock">In Stock:</form:label></td>
				<td><form:input path="inStock" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="avlQty">Available Qty:</form:label></td>
				<td><form:input path="avlQty" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="minOrderQty">Min Order Qty:</form:label></td>
				<td><form:input path="minOrderQty" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="qtyPerBox">Qty Per Box:</form:label></td>
				<td><form:input path="qtyPerBox" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<td><form:label path="reorderPoint">Reorder Point:</form:label></td>
				<td><form:input path="reorderPoint" size="30" maxlength="30"></form:input></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" class="blue-button" /></td>
			</tr>
		</table>
	</form:form>
	</br>
	<h3>Inventory List</h3>
	<c:if test="${!empty listOfInventories}">
		<table class="tg">
			<tr>
				<th width="80">Id</th>
				<th width="120">Item Name</th>
				<th width="60">size</th>
				<th width="60">Type</th>
				<th width="60">In Stock</th>
				<th width="60">Available Qty</th>
				<th width="60">Min Order Qty</th>
				<th width="60">Qty Per Box</th>
				<th width="60">Reorder Point</th>
				<th width="60">Edit</th>
				<th width="60">Delete</th>
			</tr>
			<c:forEach items="${listOfInventories}" var="inventory">
				<tr>
					<td>${inventory.id}</td>
					<td>${inventory.itemName}</td>
					<td>${inventory.size}</td>
					<td>${inventory.type}</td>
					<td>${inventory.inStock}</td>
					<td>${inventory.avlQty}</td>
					<td>${inventory.minOrderQty}</td>
					<td>${inventory.qtyPerBox}</td>
					<td>${inventory.reorderPoint}</td>
					<td><a
						href="<c:url value='/updateInventory/${inventory.id}' />">Edit</a></td>
					<td><a
						href="<c:url value='/deleteInventory/${inventory.id}' />">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>
