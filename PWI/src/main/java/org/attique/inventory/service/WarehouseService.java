package org.attique.inventory.service;

import java.util.List;

import org.attique.inventory.dao.CountryDAO;
import org.attique.inventory.dao.WarehouseDao;
import org.attique.inventory.model.Country;
import org.attique.inventory.model.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("warehouseService")
public class WarehouseService {

	@Autowired
	WarehouseDao warehouseDao;
	
	@Transactional
	public List<Warehouse> getAllWarehouses() {
		return warehouseDao.getAllWarehouses();
	}

	@Transactional
	public Warehouse getWarehouse(int id) {
		return warehouseDao.getWarehouse(id);
	}

	@Transactional
	public void addWarehouse(Warehouse warehouse) {
		warehouseDao.addWarehouse(warehouse);
	}

	@Transactional
	public void updateWarehouse(Warehouse warehouse) {
		warehouseDao.updateWarehouse(warehouse);

	}

	@Transactional
	public void deleteWarehouse(int id) {
		warehouseDao.deleteWarehouse(id);
	}
}
