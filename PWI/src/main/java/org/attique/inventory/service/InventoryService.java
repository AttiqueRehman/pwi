package org.attique.inventory.service;

import java.util.List;

import org.attique.inventory.dao.InventoryDao;
import org.attique.inventory.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("inventoryService")
public class InventoryService {

	@Autowired
	InventoryDao inventoryDao;
	
	@Transactional
	public List<Inventory> getAllInventory() {
		return inventoryDao.getAllInventory();
	}

	@Transactional
	public Inventory getInventory(int id) {
		return inventoryDao.getInventory(id);
	}

	@Transactional
	public void addInventory(Inventory inventory) {
		inventoryDao.addInventory(inventory);
	}

	@Transactional
	public void updateInventory(Inventory inventory) {
		inventoryDao.updateInventory(inventory);

	}

	@Transactional
	public void deleteInventory(int id) {
		inventoryDao.deleteInventory(id);
	}
}
