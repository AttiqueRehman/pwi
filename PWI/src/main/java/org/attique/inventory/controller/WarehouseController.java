package org.attique.inventory.controller;


import java.util.List;

import org.attique.inventory.model.Warehouse;
import org.attique.inventory.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WarehouseController {
	
	@Autowired
	WarehouseService warehouseService;
	
	@RequestMapping(value = "/getAllWarehouses", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getWarehouses(Model model) {
		
		List<Warehouse> listOfWarehouses = warehouseService.getAllWarehouses();
		model.addAttribute("warehouse", new Warehouse());
		model.addAttribute("listOfWarehouses", listOfWarehouses);
		return "warehouseDetails";
	}

	@RequestMapping(value = "/getWarehouse/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Warehouse getWarehouseById(@PathVariable int id) {
		return warehouseService.getWarehouse(id);
	}

	@RequestMapping(value = "/addWarehouse", method = RequestMethod.POST, headers = "Accept=application/json")
	public String addWarehouse(@ModelAttribute("warehouse") Warehouse warehouse) {	
		if(warehouse.getId()==0)
		{
			warehouseService.addWarehouse(warehouse);
		}
		else
		{	
			warehouseService.updateWarehouse(warehouse);
		}
		
		return "redirect:/getAllWarehouses";
	}

	@RequestMapping(value = "/updateWarehouse/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String updateWarehouse(@PathVariable("id") int id,Model model) {
		 model.addAttribute("warehouse", this.warehouseService.getWarehouse(id));
	        model.addAttribute("listOfWarehouses", this.warehouseService.getAllWarehouses());
	        return "warehouseDetails";
	}

	@RequestMapping(value = "/deleteWarehouse/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String deleteWarehouse(@PathVariable("id") int id) {
		warehouseService.deleteWarehouse(id);
		 return "redirect:/getAllWarehouse";

	}	
}
