package org.attique.inventory.controller;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.attique.inventory.model.Inventory;
import org.attique.inventory.model.ProductType;
import org.attique.inventory.model.Warehouse;
import org.attique.inventory.service.InventoryService;
import org.attique.inventory.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class InventoryController {
	
	@Autowired
	InventoryService inventoryService;
	
	@RequestMapping(value = "/getAllInventory", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getInventory(Model model) {
		
		List<Inventory> listOfInventories = inventoryService.getAllInventory();
		List<ProductType> productList	=	new ArrayList<ProductType>(Arrays.asList(ProductType.values()));
		model.addAttribute("productList", productList);
		model.addAttribute("inventory", new Inventory());
		model.addAttribute("listOfInventories", listOfInventories);
		return "inventoryDetails";
	}

	@RequestMapping(value = "/getInventory/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Inventory getInventoryById(@PathVariable int id) {
		return inventoryService.getInventory(id);
	}

	@RequestMapping(value = "/addInventory", method = RequestMethod.POST, headers = "Accept=application/json")
	public String addInventory(@ModelAttribute("inventory") Inventory inventory) {	
		if(inventory.getId()==0)
		{
			inventoryService.addInventory(inventory);
		}
		else
		{	
			inventoryService.updateInventory(inventory);
		}
		
		return "redirect:/getAllInventory";
	}

	@RequestMapping(value = "/updateInventory/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String updateInventory(@PathVariable("id") int id, Model model) {
		 model.addAttribute("inventory", this.inventoryService.getInventory(id));
	        model.addAttribute("listOfInventories", this.inventoryService.getAllInventory());
	        return "inventoryDetails";
	}

	@RequestMapping(value = "/deleteInventory/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String deleteInventory(@PathVariable("id") int id) {
		inventoryService.deleteInventory(id);
		 return "redirect:/getAllInventory";

	}	
}
