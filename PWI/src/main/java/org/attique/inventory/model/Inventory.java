package org.attique.inventory.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * This is our model class and it corresponds to Country table in database
 */
@Entity
@Table(name="INVENTORY")
public class Inventory{
	
	@Id
	@Column(name="inventoryId")
	@GeneratedValue
	int id;
	
	@Column(name = "warehouse", nullable = false)
	String warehouse;

	
	@Column(name="itemName")
	String itemName;	
	

	@Column(name="size")
	String size;

	@Enumerated(EnumType.STRING)
	ProductType type;
	
	@Column(name="inStock")
	int inStock;
	
	@Column(name="avlQty")
	int avlQty;
	
	@Column(name="minOrdrQty")
	int minOrderQty;
	
	@Column(name="qtyPerBox")
	int qtyPerBox;
	
	@Column(name="reoderPoint")
	int reorderPoint;

	
	

	
	
	
	public ProductType getType() {
		return type;
	}







	public void setType(ProductType type) {
		this.type = type;
	}







	public int getId() {
		return id;
	}







	public void setId(int id) {
		this.id = id;
	}







	public String getWarehouse() {
		return warehouse;
	}







	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}







	public String getItemName() {
		return itemName;
	}







	public void setItemName(String itemName) {
		this.itemName = itemName;
	}







	public String getSize() {
		return size;
	}







	public void setSize(String size) {
		this.size = size;
	}







	public int getInStock() {
		return inStock;
	}







	public void setInStock(int inStock) {
		this.inStock = inStock;
	}







	public int getAvlQty() {
		return avlQty;
	}







	public void setAvlQty(int avlQty) {
		this.avlQty = avlQty;
	}







	public int getMinOrderQty() {
		return minOrderQty;
	}







	public void setMinOrderQty(int minOrderQty) {
		this.minOrderQty = minOrderQty;
	}














	public int getQtyPerBox() {
		return qtyPerBox;
	}







	public void setQtyPerBox(int qtyPerBox) {
		this.qtyPerBox = qtyPerBox;
	}







	public int getReorderPoint() {
		return reorderPoint;
	}







	public void setReorderPoint(int reorderPoint) {
		this.reorderPoint = reorderPoint;
	}







	public Inventory() {
		super();
	}
	
}