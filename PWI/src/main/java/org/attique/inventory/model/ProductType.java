package org.attique.inventory.model;

public enum ProductType {
	FINISHED_PRODUCT("Finish Product"),
	COMPONENT("Component"),
	PACKAGING_MATERIAL("Packaging Material");
	
	private String type;
	
	private ProductType(String t) {
		type	=	 t;
	}
	
	public String getType() {
		return type;
	}
}
