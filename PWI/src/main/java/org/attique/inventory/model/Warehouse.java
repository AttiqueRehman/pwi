package org.attique.inventory.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

/*
 * This is our model class and it corresponds to Warehouse table in database
 */
@Entity
@Table(name="WAREHOUSE")
public class Warehouse{
	
	@Id
	@Column(name="warehouseId")
	@GeneratedValue
	int id;
	
	@Column(name="warehouseName")
	String warehouseName;	
	

	@Column(name="address")
	String address;
	
	@Column(name = "country", nullable = false)
	String country;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "Warehouse_inventory", joinColumns = {@JoinColumn(name = "warehouseId")}, inverseJoinColumns = {@JoinColumn(name = "inventoryId")})
	private Set<Inventory> inventoryItems	=	 new HashSet<Inventory>(0);


	
	
	public Set<Inventory> getInventoryItems() {
		return inventoryItems;
	}





	public void setInventoryItems(Set<Inventory> inventoryItems) {
		this.inventoryItems = inventoryItems;
	}





	public String getCountry() {
		return country;
	}





	public void setCountry(String country) {
		this.country = country;
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public String getWarehouseName() {
		return warehouseName;
	}





	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}





	public String getAddress() {
		return address;
	}





	public void setAddress(String address) {
		this.address = address;
	}





	public Warehouse() {
		super();
	}
	
}