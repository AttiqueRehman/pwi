package org.attique.inventory.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.attique.inventory.model.Inventory;
import org.attique.inventory.model.Warehouse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WarehouseDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	InventoryDao inventoryDao;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Warehouse> getAllWarehouses() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Warehouse> warehouseList = session.createQuery("from Warehouse").list();
		System.out.println(warehouseList);
		warehouseList.forEach(warehouse -> {
			warehouse.getInventoryItems().addAll(inventoryDao.getInventoryByWarehouseId(String.valueOf(warehouse.getId())));
		});
		
		return warehouseList;
	}
	
	public Set<Warehouse> getWarehousesByCountryId(String countryId){
		Session session = this.sessionFactory.getCurrentSession();
		Set<Warehouse> warehouses	=	 new HashSet<Warehouse>();
		List<Warehouse> warehouseList = session.createQuery("from Warehouse").list();
		warehouseList.forEach(warehouse -> {
			if(warehouse.getCountry().equals(countryId)) {
				warehouses.add(warehouse);
			}
		});
		return warehouses;
	}

	public Warehouse getWarehouse(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Warehouse warehouse = (Warehouse) session.get(Warehouse.class, new Integer(id));
		return warehouse;
	}

	public Warehouse addWarehouse(Warehouse warehouse) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(warehouse);
		return warehouse;
	}

	public void updateWarehouse(Warehouse warehouse) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(warehouse);
	}

	public void deleteWarehouse(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Warehouse p = (Warehouse) session.load(Warehouse.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}	
}
