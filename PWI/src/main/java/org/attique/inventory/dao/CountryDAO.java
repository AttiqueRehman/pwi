package org.attique.inventory.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.attique.inventory.model.Country;
import org.attique.inventory.model.Warehouse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CountryDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	WarehouseDao warehouseDao;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Country> getAllCountries() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Country> countryList = session.createQuery("from Country").list();
		countryList.forEach(country -> {
			country.getWarehouses().addAll(warehouseDao.getWarehousesByCountryId(String.valueOf(country.getId())));
		});
		return countryList;
	}

	public Country getCountry(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Country country = (Country) session.get(Country.class, new Integer(id));
		return country;
	}

	public Country addCountry(Country country) {
		List<Warehouse> allWarehouses = warehouseDao.getAllWarehouses();
		country.getWarehouses().addAll(allWarehouses.stream()
				.filter(warehouse -> warehouse.getCountry().equals(country.getId())).collect(Collectors.toList()));
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(country);
		return country;
	}

	public void updateCountry(Country country) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(country);
	}

	public void deleteCountry(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Country p = (Country) session.load(Country.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}
}
