package org.attique.inventory.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.attique.inventory.model.Inventory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	CountryDAO countryDao;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	public List<Inventory> getAllInventory() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Inventory> inventoryList = session.createQuery("from Inventory").list();
		return inventoryList;
	}
	
	public Set<Inventory> getInventoryByWarehouseId(String warehouseId){
		Session session = this.sessionFactory.getCurrentSession();
		Set<Inventory> inventory	=	 new HashSet<Inventory>();
		List<Inventory> inventoryList = session.createQuery("from Inventory").list();
		inventoryList.forEach(inv -> {
			if(inv.getWarehouse().equals(warehouseId)) {
				inventory.add(inv);
			}
		});
		return inventory;
	}

	public Inventory getInventory(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Inventory inventory = (Inventory) session.get(Inventory.class, new Integer(id));
		return inventory;
	}

	public Inventory addInventory(Inventory inventory) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(inventory);
		return inventory;
	}

	public void updateInventory(Inventory inventory) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(inventory);
	}

	public void deleteInventory(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Inventory p = (Inventory) session.load(Inventory.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}	
}
